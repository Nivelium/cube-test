import parseISO from 'date-fns/parseISO/index.js'
import { PostgresWriter } from './postgres-writer';
import _ from 'lodash'

export interface IEvent {
	[index: string]: any;
	msg: string;
}

export interface IColumnMetadata extends Map<string, ETypes> {}

export interface ITableMetadata {
	fields: IColumnMetadata
}

export interface ITablesMetadata extends Map<string, ITableMetadata> {}

export enum ETypes {
	TEXT,
	NUMERIC,
	TIMESTAMPTZ,
	BOOLEAN,
	JSON,
}

export class EventParser {
	private readonly repository: PostgresWriter;

	static getFieldsTypesMap(fields: Omit<IEvent, 'msg'>): IColumnMetadata {
		return new Map(Object.entries(fields)
			.map(([field, value]) => [field, EventParser.guessType(value)]))
	}

	static guessType(value: any): ETypes {
		if (typeof value === 'string') {
			if (parseISO(value) instanceof Date && !isNaN(parseISO(value).getTime())) {
				return ETypes.TIMESTAMPTZ
			}

			return ETypes.TEXT
		}

		if (typeof value === 'object') {
			return ETypes.JSON
		}

		if (typeof value === 'number') {
			return ETypes.NUMERIC
		}

		if (typeof value === 'boolean') {
			return ETypes.BOOLEAN
		}

		throw new Error(`Couldn't guess the type`)
	}

	constructor(postgresWriter: PostgresWriter) {
		this.repository = postgresWriter
	}

	async parse(event: IEvent) {
		const {
			msg,
		    ...fields
		} = event;

		const tableName = _.snakeCase(msg)
		const fieldsMetadata = EventParser.getFieldsTypesMap(fields)

		await this.repository.mergeMetadataForTable(tableName, fieldsMetadata)
		await this.repository.insert(tableName, fields)
	}
}