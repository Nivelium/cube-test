import express from 'express'
import { PostgresWriter } from './postgres-writer.js'
import { EventParser } from './event-parser.js';

const app = express();
const pgWriter = new PostgresWriter()
const eventParser = new EventParser(pgWriter)

app.use(express.json());
app.use(express.urlencoded({ extended: false }))

app.post('/',async (req, res) => {
	for (let event of req.body) {
		await eventParser.parse(event)
	}
})

pgWriter.initMetadata()
	.then(() => {
		app.listen(8081)
	})