import PQueue from 'p-queue';

export interface IOperationAggregator {
	addCreateOperation(groupName: string, operation: () => Promise<any>): Promise<void>;
	addAlterOperation(groupName: string, operation: () => Promise<any>): Promise<void>;
	addDMLOperation(groupName: string, operation: () => Promise<any>): Promise<void>;
}

const concurrency = 1;

export class OperationAggregator implements IOperationAggregator {
	private readonly queueMap: Map<string, PQueue>;

	constructor() {
		this.queueMap = new Map();
	}

	async addDMLOperation(tableName: string, operation: () => void): Promise<any> {
		this.setupTable(tableName)
		return this.addOperationWithPriority(tableName, 0, operation)
	}

	async addAlterOperation(tableName: string, operation: () => void): Promise<any> {
		this.setupTable(tableName)
		return this.addOperationWithPriority(tableName, 1, operation)
	}

	async addCreateOperation(tableName: string, operation: () => void): Promise<any> {
		this.setupTable(tableName)
		return this.addOperationWithPriority(tableName, 2, operation)
	}

	private setupTable(tableName: string) {
		if (!this.queueMap.has(tableName)) {
			this.queueMap.set(tableName, new PQueue({
				concurrency,
			}))
			const queue = this.queueMap.get(tableName);
		}
	}

	private async addOperationWithPriority(tableName: string, priority: number, operation: () => void, ) {
		const queue = this.queueMap.get(tableName);

		return queue!.add(operation, {
			priority,
		})
	}
}