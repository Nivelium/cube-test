import pg from 'pg';
import { ETypes, EventParser } from './event-parser.js';
import { IOperationAggregator, OperationAggregator } from './operation-aggregator.js';

export class PostgresWriter {
	private static readonly schema: string = 'event_collector'

	private readonly pool: pg.Pool;
	private metadata: Map<string, Map<string, ETypes>>;
	private operationAggregator: IOperationAggregator;

	static databaseDatatypeAdapt(data_type: string): ETypes {
		switch(data_type) {
			case 'numeric': return ETypes.NUMERIC
			case 'json': return ETypes.JSON
			case 'text': return ETypes.TEXT
			case 'timestamp with time zone': return ETypes.TIMESTAMPTZ
			case 'boolean': return ETypes.BOOLEAN
			default: throw new Error('Someone used his hands to modify metadata...')
		}
	}

	async initMetadata() {
		const { rows } = await this.pool.query(`
			select table_name, column_name,data_type
			from information_schema.columns
			where table_schema = 'event_collector'
			order by table_name, ordinal_position
		`)

		rows.forEach((row: any) => {
			const dataType = PostgresWriter.databaseDatatypeAdapt(row.data_type)

			this.metadata.has(row.table_name)
				? this.metadata.get(row.table_name)!.set(row.column_name, dataType)
				: this.metadata.set(row.table_name, new Map([[row.column_name, dataType]]))
		})
	}

	constructor() {
		this.pool = new pg.Pool({
			host: 'localhost',
			port: 5433,
			password: 'example',
			user: 'postgres',
			database: 'postgres'
		})
		this.metadata = new Map();
		this.operationAggregator = new OperationAggregator()
	}

	async insert(tableName: string, event: { [index: string]: any }) {

		const columns = Object.keys(event).map(event => `"${event}"`).join(',')
		const placeholders = Object.values(event).map((k, index) => `$${index + 1}`).join(',')
		const query = `
				INSERT INTO ${PostgresWriter.schema}.${tableName}
				(${columns})
				VALUES (${placeholders})
			`

		const values = Object.values(event).map((value) => {
			if (EventParser.guessType(value) === ETypes.JSON) return JSON.stringify(value)
			return value
		})
		try {
			await this.operationAggregator.addDMLOperation(tableName, () => this.pool.query(query, values))
		} catch (e) {
			console.log(e)
		}
	}

	async createTableIfNotExists(tableName: string, fieldsMetadata: Map<string, ETypes>) {
		if (this.metadata.has(tableName)) return;

		const fields = [];

		for (let fieldMetadata of fieldsMetadata.entries()) {
			fields.push(fieldMetadata)

		}

		const fieldTextPart = fields.map(([name, type ]) => {
			return `"${name}" ${ETypes[type]}`
		}).join(',\n')

		const queryText = `
			CREATE TABLE IF NOT EXISTS ${PostgresWriter.schema}.${tableName} (
		    	${fieldTextPart}   
			)
		`

		await this.pool.query(queryText)
		this.metadata.set(tableName, new Map(fields))
	}

	async addColumn(tableName: string, columnName: string, columnType: ETypes) {
		const queryText = `
			ALTER TABLE ${PostgresWriter.schema}.${tableName}
			ADD COLUMN "${columnName}" ${ETypes[columnType]};
		`
		await this.pool.query(queryText)
		this.metadata.get(tableName)!.set(columnName, columnType)

	}

	async mergeMetadataForTable(tableName: string, tableNewMetadata: Map<string, ETypes>) {
		const tableCurrentMetadata = this.metadata.get(tableName);

		if (tableCurrentMetadata) {
			for (let [field, type] of tableNewMetadata.entries()) {
				if (!tableCurrentMetadata.has(field)) {
					try {
						await this.operationAggregator.addAlterOperation(tableName, () => this.addColumn(tableName, field, type))
					} catch (e) {
						console.log(e)
					}
				}
			}
		} else {
			try {
				await this.operationAggregator.addCreateOperation(tableName, () => this.createTableIfNotExists(tableName, tableNewMetadata))
			} catch (e) {
				console.log(e);
			}
		}
	}
}