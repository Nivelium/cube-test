/* globals window */
import { ApolloClient } from 'apollo-client';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { SchemaLink } from 'apollo-link-schema';
import { makeExecutableSchema } from 'graphql-tools';
const cache = new InMemoryCache();
const defaultDashboardItems = [{
  "vizState": "{\"chartType\":\"line\",\"pivotConfig\":{\"x\":[\"RestApiRequest.time.minute\"],\"y\":[\"measures\"],\"fillMissingDates\":true,\"joinDateRange\":false},\"query\":{\"measures\":[\"RestApiRequest.countMeasure\"],\"timeDimensions\":[{\"dimension\":\"RestApiRequest.time\",\"granularity\":\"minute\",\"dateRange\":\"Last 7 days\"}],\"order\":{\"RestApiRequest.time\":\"asc\"}}}",
  "name": "Request per minute chart",
  "id": "2",
  "layout": "{\"x\":0,\"y\":0,\"w\":4,\"h\":8}"
},
  {
    "vizState": "{\"chartType\":\"line\",\"pivotConfig\":{\"x\":[\"QueryCompleted.sentat.minute\"],\"y\":[\"measures\"],\"fillMissingDates\":true,\"joinDateRange\":false},\"query\":{\"measures\":[\"QueryCompleted.count\"],\"timeDimensions\":[{\"dimension\":\"QueryCompleted.sentat\",\"granularity\":\"minute\"}],\"order\":{\"QueryCompleted.sentat\":\"asc\"},\"filters\":[]}}",
    "name": "Database queries per minute chart",
    "id": "3",
    "layout": "{\"x\":0,\"y\":16,\"w\":4,\"h\":8}"
  },
  {
    "vizState": "{\"chartType\":\"table\",\"pivotConfig\":{\"x\":[\"QueryCompleted.businessId\"],\"y\":[\"measures\"],\"fillMissingDates\":true,\"joinDateRange\":false},\"query\":{\"measures\":[\"QueryCompleted.duration\"],\"timeDimensions\":[{\"dimension\":\"QueryCompleted.sentat\"}],\"order\":{\"QueryCompleted.duration\":\"desc\"},\"limit\": 10,\"dimensions\":[\"QueryCompleted.businessId\"]}}",
    "name": "Top 10 SQL queries by total execution time.",
    "id": "4",
    "layout": "{\"x\":0,\"y\":8,\"w\":4,\"h\":8}"
  }]

const getDashboardItems = () =>
  JSON.parse(window.localStorage.getItem('dashboardItems')) ||
  defaultDashboardItems;

const setDashboardItems = (items) => {
  console.log('items', items)
  window.localStorage.setItem('dashboardItems', JSON.stringify(items));
}


const nextId = () => {
  const currentId =
    parseInt(window.localStorage.getItem('dashboardIdCounter'), 10) || 1;
  window.localStorage.setItem('dashboardIdCounter', currentId + 1);
  return currentId.toString();
};

const toApolloItem = (i) => ({ ...i, __typename: 'DashboardItem' });

const typeDefs = `
  type DashboardItem {
    id: String!
    layout: String
    vizState: String
    name: String
  }

  input DashboardItemInput {
    layout: String
    vizState: String
    name: String
  }

  type Query {
    dashboardItems: [DashboardItem]
    dashboardItem(id: String!): DashboardItem
  }

  type Mutation {
    createDashboardItem(input: DashboardItemInput): DashboardItem
    updateDashboardItem(id: String!, input: DashboardItemInput): DashboardItem
    deleteDashboardItem(id: String!): DashboardItem
  }
`;
const schema = makeExecutableSchema({
  typeDefs,
  resolvers: {
    Query: {
      dashboardItems() {
        const dashboardItems = getDashboardItems();
        return dashboardItems.map(toApolloItem);
      },

      dashboardItem(_, { id }) {
        const dashboardItems = getDashboardItems();
        return toApolloItem(dashboardItems.find((i) => i.id.toString() === id));
      },
    },
    Mutation: {
      createDashboardItem: (_, { input: { ...item } }) => {
        const dashboardItems = getDashboardItems();
        item = { ...item, id: nextId(), layout: JSON.stringify({}) };
        dashboardItems.push(item);
        setDashboardItems(dashboardItems);
        return toApolloItem(item);
      },
      updateDashboardItem: (_, { id, input: { ...item } }) => {
        const dashboardItems = getDashboardItems();
        item = Object.keys(item)
          .filter((k) => !!item[k])
          .map((k) => ({
            [k]: item[k],
          }))
          .reduce((a, b) => ({ ...a, ...b }), {});
        const index = dashboardItems.findIndex((i) => i.id.toString() === id);
        dashboardItems[index] = { ...dashboardItems[index], ...item };
        setDashboardItems(dashboardItems);
        return toApolloItem(dashboardItems[index]);
      },
      deleteDashboardItem: (_, { id }) => {
        const dashboardItems = getDashboardItems();
        const index = dashboardItems.findIndex((i) => i.id.toString() === id);
        const [removedItem] = dashboardItems.splice(index, 1);
        setDashboardItems(dashboardItems);
        return toApolloItem(removedItem);
      },
    },
  },
});
export default new ApolloClient({
  cache,
  link: new SchemaLink({
    schema,
  }),
});
