cube(`AddedToQueue`, {
  sql: `SELECT * FROM event_collector.added_to_queue`,
  
  joins: {
    
  },
  
  measures: {
    count: {
      type: `count`,
      drillMembers: [id, requestid, timestamp]
    }
  },
  
  dimensions: {
    id: {
      sql: `id`,
      type: `string`,
      primaryKey: true
    },
    
    querykey: {
      sql: `${CUBE}."queryKey"`,
      type: `string`
    },
    
    queueprefix: {
      sql: `${CUBE}."queuePrefix"`,
      type: `string`
    },
    
    requestid: {
      sql: `${CUBE}."requestId"`,
      type: `string`
    },
    
    timestamp: {
      sql: `timestamp`,
      type: `time`
    },
    
    sentat: {
      sql: `${CUBE}."sentAt"`,
      type: `time`
    }
  },
  
  dataSource: `default`
});
