cube(`ExecutingSql`, {
  sql: `SELECT * FROM event_collector.executing_sql`,
  
  joins: {
    
  },
  
  measures: {
    count: {
      type: `count`,
      drillMembers: [id, requestid, timestamp]
    }
  },
  
  dimensions: {
    query: {
      sql: `query`,
      type: `string`
    },
    
    id: {
      sql: `id`,
      type: `string`,
      primaryKey: true
    },
    
    values: {
      sql: `values`,
      type: `string`
    },
    
    querykey: {
      sql: `${CUBE}."queryKey"`,
      type: `string`
    },
    
    requestid: {
      sql: `${CUBE}."requestId"`,
      type: `string`
    },
    
    timestamp: {
      sql: `timestamp`,
      type: `time`
    },
    
    sentat: {
      sql: `${CUBE}."sentAt"`,
      type: `time`
    }
  },
  
  dataSource: `default`
});
