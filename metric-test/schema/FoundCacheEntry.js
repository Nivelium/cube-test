cube(`FoundCacheEntry`, {
  sql: `SELECT * FROM event_collector.found_cache_entry`,
  
  joins: {
    
  },
  
  measures: {
    count: {
      type: `count`,
      drillMembers: [id, requestid, timestamp]
    }
  },
  
  dimensions: {
    id: {
      sql: `id`,
      type: `string`,
      primaryKey: true
    },
    
    requestid: {
      sql: `${CUBE}."requestId"`,
      type: `string`
    },
    
    renewalkey: {
      sql: `${CUBE}."renewalKey"`,
      type: `string`
    },
    
    cachekey: {
      sql: `${CUBE}."cacheKey"`,
      type: `string`
    },
    
    newrenewalkey: {
      sql: `${CUBE}."newRenewalKey"`,
      type: `string`
    },
    
    sentat: {
      sql: `${CUBE}."sentAt"`,
      type: `time`
    },
    
    timestamp: {
      sql: `timestamp`,
      type: `time`
    }
  },
  
  dataSource: `default`
});
