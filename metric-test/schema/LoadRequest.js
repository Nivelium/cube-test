cube(`LoadRequest`, {
  sql: `SELECT * FROM event_collector.load_request`,
  
  joins: {
    
  },
  
  measures: {
    count: {
      type: `count`,
      drillMembers: [requestid, id, timestamp]
    }
  },
  
  dimensions: {
    requestid: {
      sql: `${CUBE}."requestId"`,
      type: `string`
    },
    
    id: {
      sql: `id`,
      type: `string`,
      primaryKey: true
    },
    
    query: {
      sql: `query`,
      type: `string`
    },
    
    securitycontext: {
      sql: `${CUBE}."securityContext"`,
      type: `string`
    },
    
    timestamp: {
      sql: `timestamp`,
      type: `time`
    },
    
    sentat: {
      sql: `${CUBE}."sentAt"`,
      type: `time`
    }
  },
  
  dataSource: `default`
});
