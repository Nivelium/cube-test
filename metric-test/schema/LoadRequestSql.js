cube(`LoadRequestSql`, {
  sql: `SELECT * FROM event_collector.load_request_sql`,
  
  joins: {
    
  },
  
  measures: {
    count: {
      type: `count`,
      drillMembers: [id, requestid, timestamp]
    },
    
    duration: {
      sql: `duration`,
      type: `sum`
    }
  },
  
  dimensions: {
    id: {
      sql: `id`,
      type: `string`,
      primaryKey: true
    },
    
    query: {
      sql: `query`,
      type: `string`
    },
    
    requestid: {
      sql: `${CUBE}."requestId"`,
      type: `string`
    },
    
    securitycontext: {
      sql: `${CUBE}."securityContext"`,
      type: `string`
    },
    
    sqlquery: {
      sql: `${CUBE}."sqlQuery"`,
      type: `string`
    },
    
    sentat: {
      sql: `${CUBE}."sentAt"`,
      type: `time`
    },
    
    timestamp: {
      sql: `timestamp`,
      type: `time`
    }
  },
  
  dataSource: `default`
});
