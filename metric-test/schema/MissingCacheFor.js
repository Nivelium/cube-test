cube(`MissingCacheFor`, {
  sql: `SELECT * FROM event_collector.missing_cache_for`,
  
  joins: {
    
  },
  
  measures: {
    count: {
      type: `count`,
      drillMembers: [requestid, id, timestamp]
    }
  },
  
  dimensions: {
    requestid: {
      sql: `${CUBE}."requestId"`,
      type: `string`
    },
    
    cachekey: {
      sql: `${CUBE}."cacheKey"`,
      type: `string`
    },
    
    id: {
      sql: `id`,
      type: `string`,
      primaryKey: true
    },
    
    sentat: {
      sql: `${CUBE}."sentAt"`,
      type: `time`
    },
    
    timestamp: {
      sql: `timestamp`,
      type: `time`
    }
  },
  
  dataSource: `default`
});
