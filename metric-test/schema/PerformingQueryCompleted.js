cube(`PerformingQueryCompleted`, {
  sql: `SELECT * FROM event_collector.performing_query_completed`,
  
  joins: {
    
  },
  
  measures: {
    count: {
      type: `count`,
      drillMembers: [id, requestid, timestamp]
    },
    
    duration: {
      sql: `duration`,
      type: `sum`
    }
  },
  
  dimensions: {
    id: {
      sql: `id`,
      type: `string`,
      primaryKey: true
    },
    
    requestid: {
      sql: `${CUBE}."requestId"`,
      type: `string`
    },
    
    queueprefix: {
      sql: `${CUBE}."queuePrefix"`,
      type: `string`
    },
    
    querykey: {
      sql: `${CUBE}."queryKey"`,
      type: `string`
    },
    
    sentat: {
      sql: `${CUBE}."sentAt"`,
      type: `time`
    },
    
    timestamp: {
      sql: `timestamp`,
      type: `time`
    }
  },
  
  dataSource: `default`
});
