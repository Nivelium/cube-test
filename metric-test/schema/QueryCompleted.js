cube(`QueryCompleted`, {
  sql: `SELECT * FROM event_collector.query_completed`,
  
  joins: {
    
  },

  measures: {
    count: {
      type: `count`,
      drillMembers: [requestid, id, timestamp]
    },
    
    duration: {
      sql: `duration`,
      type: `sum`
    }
  },
  
  dimensions: {
    query: {
      sql: `query`,
      type: `string`
    },
    
    params: {
      sql: `params`,
      type: `string`
    },
    
    requestid: {
      sql: `${CUBE}."requestId"`,
      type: `string`
    },
    
    id: {
      sql: `id`,
      type: `string`,
      primaryKey: true
    },

    businessId: {
      sql: `${CUBE}.id`,
      type: `string`
    },
    
    sentat: {
      sql: `${CUBE}."sentAt"`,
      type: `time`
    },
    
    timestamp: {
      sql: `timestamp`,
      type: `time`
    },

    durationDimension: {
      sql: `duration`,
      type: `number`
    }
  },
  
  dataSource: `default`
});
