cube(`QueryDuration`, {
  sql: `SELECT * 
        FROM event_collector.query_completed 
        order by event_collector.query_completed.duration desc limit 10`,
  
  joins: {
    
  },

  measures: {
    duration: {
      sql: `duration`,
      type: `sum`
    }
  },
  
  dimensions: {
    query: {
      sql: `query`,
      type: `string`
    },
    
    params: {
      sql: `params`,
      type: `string`
    },
    
    requestid: {
      sql: `${CUBE}."requestId"`,
      type: `string`
    },

    customId: {
      sql: `${CUBE}."id"`,
      type: `string`
    },
    
    id: {
      sql: `id`,
      type: `string`,
      primaryKey: true
    },
    
    sentat: {
      sql: `${CUBE}."sentAt"`,
      type: `time`
    },
    
    timestamp: {
      sql: `timestamp`,
      type: `time`
    }
  },
  
  dataSource: `default`
});
