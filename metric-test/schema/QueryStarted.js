cube(`QueryStarted`, {
  sql: `SELECT * FROM event_collector.query_started`,
  
  joins: {
    QueryCompleted: {
      relationship: `hasOne`,
      sql: `${QueryStarted}.id = ${QueryCompleted}.id`
    }
  },
  
  measures: {
    count: {
      type: `count`,
      drillMembers: [requestid, id, timestamp]
    },
    executionDuration: {
      sql: `max(${QueryCompleted}.timestamp - ${QueryStarted}.timestamp)`,
      type: `number`
    }
  },
  
  dimensions: {
    params: {
      sql: `params`,
      type: `string`
    },
    
    requestid: {
      sql: `${CUBE}."requestId"`,
      type: `string`
    },
    
    id: {
      sql: `id`,
      type: `string`,
      primaryKey: true
    },
    
    query: {
      sql: `query`,
      type: `string`
    },
    
    sentat: {
      sql: `${CUBE}."sentAt"`,
      type: `time`
    },
    
    timestamp: {
      sql: `timestamp`,
      type: `time`
    }
  },
  
  dataSource: `default`
});
