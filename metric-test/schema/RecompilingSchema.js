cube(`RecompilingSchema`, {
  sql: `SELECT * FROM event_collector.recompiling_schema`,
  
  joins: {
    
  },
  
  measures: {
    count: {
      type: `count`,
      drillMembers: [requestid, id, timestamp]
    }
  },
  
  dimensions: {
    requestid: {
      sql: `${CUBE}."requestId"`,
      type: `string`
    },
    
    version: {
      sql: `version`,
      type: `string`
    },
    
    id: {
      sql: `id`,
      type: `string`,
      primaryKey: true
    },
    
    sentat: {
      sql: `${CUBE}."sentAt"`,
      type: `time`
    },
    
    timestamp: {
      sql: `timestamp`,
      type: `time`
    }
  },
  
  dataSource: `default`
});
