cube(`RefreshSchedulerRun`, {
  sql: `SELECT * FROM event_collector.refresh_scheduler_run`,
  
  joins: {
    
  },
  
  measures: {
    count: {
      type: `count`,
      drillMembers: [id, requestid, timestamp]
    }
  },
  
  dimensions: {
    id: {
      sql: `id`,
      type: `string`,
      primaryKey: true
    },
    
    securitycontext: {
      sql: `${CUBE}."securityContext"`,
      type: `string`
    },
    
    requestid: {
      sql: `${CUBE}."requestId"`,
      type: `string`
    },
    
    timestamp: {
      sql: `timestamp`,
      type: `time`
    },
    
    sentat: {
      sql: `${CUBE}."sentAt"`,
      type: `time`
    }
  },
  
  dataSource: `default`
});
