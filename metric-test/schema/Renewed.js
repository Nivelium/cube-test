cube(`Renewed`, {
  sql: `SELECT * FROM event_collector.renewed`,
  
  joins: {
    
  },
  
  measures: {
    count: {
      type: `count`,
      drillMembers: [requestid, id, timestamp]
    }
  },
  
  dimensions: {
    requestid: {
      sql: `${CUBE}."requestId"`,
      type: `string`
    },
    
    id: {
      sql: `id`,
      type: `string`,
      primaryKey: true
    },
    
    cachekey: {
      sql: `${CUBE}."cacheKey"`,
      type: `string`
    },
    
    sentat: {
      sql: `${CUBE}."sentAt"`,
      type: `time`
    },
    
    timestamp: {
      sql: `timestamp`,
      type: `time`
    }
  },
  
  dataSource: `default`
});
