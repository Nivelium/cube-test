cube(`RestApiRequest`, {
  sql: `SELECT * FROM event_collector.rest_api_request`,
  
  joins: {
    
  },
  
  measures: {
    countMeasure: {
      type: `count`,
      drillMembers: [requestid, id, timestamp]
    }
  },
  
  dimensions: {
    method: {
      sql: `method`,
      type: `string`
    },
    
    path: {
      sql: `path`,
      type: `string`
    },
    
    ip: {
      sql: `ip`,
      type: `string`
    },
    
    requestid: {
      sql: `${CUBE}."requestId"`,
      type: `string`
    },
    
    id: {
      sql: `id`,
      type: `string`,
      primaryKey: true
    },
    
    securitycontext: {
      sql: `${CUBE}."securityContext"`,
      type: `string`
    },
    
    time: {
      sql: `time`,
      type: `time`
    },
    
    timestamp: {
      sql: `timestamp`,
      type: `time`
    },
    
    sentat: {
      sql: `${CUBE}."sentAt"`,
      type: `time`
    }
  },
  preAggregations: {
    reqPerMin: {
      sqlAlias: 'rpm',
      type: 'rollup',
      measureReferences: [RestApiRequest.countMeasure],
      timeDimensionReference: time,
      partitionGranularity: `day`,
      granularity: `minute`
    }
  },
  dataSource: `default`
});
