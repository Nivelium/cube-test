cube(`UsingCacheFor`, {
  sql: `SELECT * FROM event_collector.using_cache_for`,
  
  joins: {
    
  },
  
  measures: {
    count: {
      type: `count`,
      drillMembers: [id, requestid, timestamp]
    }
  },
  
  dimensions: {
    id: {
      sql: `id`,
      type: `string`,
      primaryKey: true
    },
    
    cachekey: {
      sql: `${CUBE}."cacheKey"`,
      type: `string`
    },
    
    requestid: {
      sql: `${CUBE}."requestId"`,
      type: `string`
    },
    
    timestamp: {
      sql: `timestamp`,
      type: `time`
    },
    
    sentat: {
      sql: `${CUBE}."sentAt"`,
      type: `time`
    }
  },
  
  dataSource: `default`
});
