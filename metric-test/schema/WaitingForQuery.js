cube(`WaitingForQuery`, {
  sql: `SELECT * FROM event_collector.waiting_for_query`,
  
  joins: {
    
  },
  
  measures: {
    count: {
      type: `count`,
      drillMembers: [id, waitingforrequestid, requestid, timestamp]
    }
  },
  
  dimensions: {
    toprocessquerykeys: {
      sql: `${CUBE}."toProcessQueryKeys"`,
      type: `string`
    },
    
    active: {
      sql: `active`,
      type: `string`
    },
    
    id: {
      sql: `id`,
      type: `string`,
      primaryKey: true
    },
    
    waitingforrequestid: {
      sql: `${CUBE}."waitingForRequestId"`,
      type: `string`
    },
    
    queueprefix: {
      sql: `${CUBE}."queuePrefix"`,
      type: `string`
    },
    
    requestid: {
      sql: `${CUBE}."requestId"`,
      type: `string`
    },
    
    querykey: {
      sql: `${CUBE}."queryKey"`,
      type: `string`
    },
    
    activequerykeys: {
      sql: `${CUBE}."activeQueryKeys"`,
      type: `string`
    },
    
    sentat: {
      sql: `${CUBE}."sentAt"`,
      type: `time`
    },
    
    timestamp: {
      sql: `timestamp`,
      type: `time`
    }
  },
  
  dataSource: `default`
});
