cube(`WaitingForRenew`, {
  sql: `SELECT * FROM event_collector.waiting_for_renew`,
  
  joins: {
    
  },
  
  measures: {
    count: {
      type: `count`,
      drillMembers: [id, requestid, timestamp]
    }
  },
  
  dimensions: {
    cachekey: {
      sql: `${CUBE}."cacheKey"`,
      type: `string`
    },
    
    id: {
      sql: `id`,
      type: `string`,
      primaryKey: true
    },
    
    requestid: {
      sql: `${CUBE}."requestId"`,
      type: `string`
    },
    
    sentat: {
      sql: `${CUBE}."sentAt"`,
      type: `time`
    },
    
    timestamp: {
      sql: `timestamp`,
      type: `time`
    }
  },
  
  dataSource: `default`
});
